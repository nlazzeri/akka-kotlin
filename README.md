# Akka Kotlin

This is just me messing around with Akka and Kotlin

Trying out how Scala like a Kotlin implementation feels. So far its great!

### Prerequisites

jdk8

### Installing

gradlew build

## Running the tests

gradlew test

## Built With

* [Gradle](https://gradle.org/) - Build Tool


## License

This project is licensed under the MIT License

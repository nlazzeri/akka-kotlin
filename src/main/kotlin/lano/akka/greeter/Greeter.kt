package lano.akka.greeter

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.PostStop
import akka.actor.typed.javadsl.Behaviors

object Greeter {
    sealed class Protocol {
        data class Greet(val ref: ActorRef<Answer>) : Protocol()
        data class WhoToGreet(val who: String) : Protocol()
        data class Answer(val text: String)
    }

    val behavior: Behavior<Protocol> = greeterBehavior(who = "")

    private fun greeterBehavior(who: String): Behavior<Protocol> =
        Behaviors.receive<Protocol>({ context, msg ->
                when (msg) {
                    is Protocol.Greet -> {
                        msg.ref.tell(Protocol.Answer("hello $who"))
                         return@receive Behaviors.stopped()
                    }
                    is Protocol.WhoToGreet -> {
                        context.log.info("set new greeter to ${msg.who}")
                        return@receive greeterBehavior(msg.who)
                    }
                }
            },
            { context, signal ->
                when (signal) {
                    is PostStop -> context.log.info("Greeter Stopped")
                }
                 return@receive Behaviors.same()
            }
        )

}


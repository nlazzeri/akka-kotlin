package lano.akka.greeter

import akka.actor.typed.ActorSystem
import akka.actor.typed.Behavior
import akka.actor.typed.javadsl.Behaviors


fun main(args: Array<String>) {
    val ref = ActorSystem.create(Runner.behavior, "greeters")
    ref.tell(Runner.Protocol.Start)
}

object Runner {
    sealed class Protocol {
        object Start : Protocol()
        object Terminated : Protocol()
    }

    val behavior: Behavior<Protocol> = runnerBehavior()

    private fun runnerBehavior(): Behavior<Protocol> =
        Behaviors.setup { context ->
            val ref = context.spawn(Caller.behavior, "caller")
            context.watchWith(ref, Protocol.Terminated)

            return@setup Behaviors.receive { ctx, msg ->
                when (msg) {
                    Protocol.Start -> {
                        ref.tell(Caller.Protocol.Start)
                        Behaviors.same()
                    }
                    Protocol.Terminated -> {
                        val wait = 200L
                        ctx.log.debug("waiting $wait ms for logs to flush")
                        Thread.sleep(wait)
                        Behaviors.stopped()
                    }

                }
            }
        }

}
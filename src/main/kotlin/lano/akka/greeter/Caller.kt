package lano.akka.greeter

import akka.actor.typed.Behavior
import akka.actor.typed.PostStop
import akka.actor.typed.javadsl.Behaviors

object Caller {
    sealed class Protocol {
        data class WrappedAnswer(val wrapped: Greeter.Protocol.Answer) : Protocol()
        object Start : Protocol()
    }

    val behavior: Behavior<Protocol> = mainBehavior()

    private fun mainBehavior(): Behavior<Protocol> =
        Behaviors.receive({ context, msg ->
            when (msg) {
                Protocol.Start -> {
                    val ref = context.spawn(Greeter.behavior, "greeter")
                    context.log.info("send msg to greeter")
                    ref.tell(Greeter.Protocol.WhoToGreet("Norbert"))
                    val answerRef =
                        context.messageAdapter(Greeter.Protocol.Answer::class.java) { answer: Greeter.Protocol.Answer ->
                            Protocol.WrappedAnswer(answer)
                        }
                    ref.tell(Greeter.Protocol.Greet(answerRef))
                    return@receive Behaviors.same()
                }
                is Protocol.WrappedAnswer -> {
                    context.log.info("Message from Greeter: ${msg.wrapped.text}")
                    return@receive Behaviors.stopped()
                }
            }
        },
            { context, signal ->
                when (signal) {
                    is PostStop -> {
                        context.log.info("Main stopped")
                    }
                }
                Behaviors.same()
            }
        )
}
package lano.akka.devices

import akka.actor.typed.ActorRef

sealed class SharedDeviceCommands : DeviceGroup.Command, DeviceManager.Command {
    data class RequestTrackDevice(val groupId: String, val deviceId: String, val replyTo: ActorRef<DeviceRegistered>) : SharedDeviceCommands()
    data class QueryDeviceList(val requestId: Long, val groupId: String, val replyTo: ActorRef<QueryDevicesResponse>) : SharedDeviceCommands()
    data class QueryAllTemps(val requestId: Long, val groupId: String, val replyTo: ActorRef<QueryTempResponse>) : SharedDeviceCommands()

    data class DeviceRegistered(val device: ActorRef<Device.Command>)
    data class QueryDevicesResponse(val requestId: Long, val deviceList: Set<String>)
    data class QueryTempResponse(val requestId: Long, val temperatures: Map<String, TemperatureReading>)
}
sealed class TemperatureReading {
    data class Temperature(val value: Double) : TemperatureReading()
    object TemperatureNotAvailable : TemperatureReading()
    object DeviceNotAvailable : TemperatureReading()
    object DeviceTimedOut : TemperatureReading()
}
package lano.akka.devices

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.PostStop
import akka.actor.typed.javadsl.Behaviors


object Device {
    sealed class Command {
        data class ReadTemp(val requestId: Long, val replyTo: ActorRef<ReadResponse>) : Command()
        data class WriteTemp(val requestId: Long, val temp: Double, val replyTo: ActorRef<WriteAckResponse>) :
            Command()
        object Passivate : Command()
    }
    data class ReadResponse(val requestId: Long, val deviceId: String, val value: Double?)
    data class WriteAckResponse(val requestId: Long)

    fun deviceBehavior(groupId: String, deviceId: String, lastReading: Double?): Behavior<Command> =
        Behaviors.setup { setupCtx ->
            setupCtx.log.info("Device actor {}-{} started", groupId, deviceId)
            return@setup Behaviors.receive(
                { _, msg ->
                    when (msg) {
                        is Command.ReadTemp -> {
                            msg.replyTo.tell(ReadResponse(msg.requestId, deviceId, lastReading))
                            return@receive Behaviors.same()
                        }
                        is Command.WriteTemp -> {
                            msg.replyTo.tell(WriteAckResponse(msg.requestId))
                            return@receive deviceBehavior(groupId, deviceId, msg.temp)
                        }
                        Command.Passivate -> {
                            return@receive Behaviors.stopped()
                        }
                    }
                },
                { ctx, signal ->
                    when (signal) {
                        is PostStop -> {
                            ctx.log.info("Device actor {}-{} stopped", groupId, deviceId)
                        }
                    }
                    return@receive Behaviors.same()
                }
            )
        }
}
package lano.akka.devices

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.PostStop
import akka.actor.typed.javadsl.Behaviors

object DeviceManager {

    interface Command
    sealed class ManagerCommand : Command {
        data class GroupTerminated(val ref: ActorRef<DeviceGroup.Command>, val groupId: String) : ManagerCommand()
    }

    fun behavior(groupIdToActor: Map<String, ActorRef<DeviceGroup.Command>>): Behavior<Command> =
            Behaviors.setup { setupCtx ->
                setupCtx.log.info("DeviceManager started")
                Behaviors.receive(
                        { ctx, msg ->
                            when (msg) {
                                is SharedDeviceCommands.RequestTrackDevice -> {
                                    if (groupIdToActor.containsKey(msg.groupId)) {
                                        groupIdToActor[msg.groupId]!!.tell(msg)
                                        return@receive Behaviors.same()
                                    } else {
                                        val groupActor = ctx.spawn(DeviceGroup.behavior(msg.groupId, emptyMap()), "group-${msg.groupId}")
                                        ctx.watchWith(groupActor, ManagerCommand.GroupTerminated(groupActor, msg.groupId))
                                        groupActor.tell(msg)
                                        return@receive behavior(groupIdToActor.plus(Pair(msg.groupId, groupActor)))
                                    }
                                }
                                is SharedDeviceCommands.QueryDeviceList -> {
                                    if (groupIdToActor.containsKey(msg.groupId)) {
                                        groupIdToActor[msg.groupId]!!.tell(msg)
                                    } else {
                                        msg.replyTo.tell(SharedDeviceCommands.QueryDevicesResponse(msg.requestId, emptySet()))
                                    }
                                    return@receive Behaviors.same()
                                }
                                is ManagerCommand.GroupTerminated -> {
                                    ctx.log.info("Device group actor for {} has been terminated", msg.groupId)
                                        return@receive behavior(groupIdToActor.minus(msg.groupId))
                                }
                                else -> Behaviors.unhandled()
                            }
                        },
                        { ctx, signal ->
                            when(signal){
                                is PostStop -> {
                                    ctx.log.info("DeviceManager has stopped")
                                    return@receive Behaviors.same()
                                }
                                else -> return@receive Behaviors.unhandled()
                            }

                        }
                )
            }
}
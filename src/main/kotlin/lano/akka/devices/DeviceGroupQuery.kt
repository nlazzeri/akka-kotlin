package lano.akka.devices

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.PostStop
import akka.actor.typed.javadsl.Behaviors
import java.time.Duration

object DeviceGroupQuery {
    interface Command
    sealed class QueryCommand : Command {
        object QueryTimeout : QueryCommand()
        data class DeviceTerminated(val deviceId: String) : QueryCommand()
        data class WrappedReadResponse(val resp: Device.ReadResponse) : QueryCommand()
    }

    fun behavior(requestId: Long,
                 requester: ActorRef<SharedDeviceCommands.QueryTempResponse>,
                 deviceIdToActor: Map<String, ActorRef<Device.Command>>,
                 timeout: Duration
    ): Behavior<Command> {
        return Behaviors.setup<Command> { setupCtx ->
            Behaviors.withTimers { timer ->
                timer.startSingleTimer(QueryCommand.QueryTimeout, QueryCommand.QueryTimeout, timeout)

                val ref = setupCtx.messageAdapter(Device.ReadResponse::class.java) { resp ->
                    QueryCommand.WrappedReadResponse(resp)
                }
                deviceIdToActor.entries.forEachIndexed { idx, entry ->
                    setupCtx.watchWith(entry.value, QueryCommand.DeviceTerminated(entry.key))
                    entry.value.tell(Device.Command.ReadTemp(idx.toLong(), ref))
                }

                return@withTimers nextBehavior(requestId, requester, deviceIdToActor, emptyMap(), deviceIdToActor.keys)
            }
        }
    }

    private fun nextBehavior(requestId: Long,
                             requester: ActorRef<SharedDeviceCommands.QueryTempResponse>,
                             deviceIdToActor: Map<String, ActorRef<Device.Command>>,
                             replies: Map<String, TemperatureReading>,
                             pending: Set<String>): Behavior<Command> {
        return if (pending.isEmpty()) {
            requester.tell(SharedDeviceCommands.QueryTempResponse(requestId, replies))
            Behaviors.stopped()
        } else {
            afterSetup(requestId, requester, deviceIdToActor, replies, pending)
        }
    }

    private fun afterSetup(requestId: Long,
                           requester: ActorRef<SharedDeviceCommands.QueryTempResponse>,
                           deviceIdToActor: Map<String, ActorRef<Device.Command>>,
                           replies: Map<String, TemperatureReading>,
                           pending: Set<String>): Behavior<Command> {
        fun nextBehaviorCurr(currReplies: Map<String, TemperatureReading>, currPending: Set<String>) =
                fun(): Behavior<Command> {
                    return nextBehavior(requestId, requester, deviceIdToActor,
                            currReplies, currPending)
                }
        return Behaviors.receive(
                { ctx, msg ->
                    when (msg) {
                        is QueryCommand.WrappedReadResponse -> {
                            if (msg.resp.value != null) {
                                return@receive nextBehaviorCurr(
                                        replies.plus(Pair(msg.resp.deviceId, TemperatureReading.Temperature(msg.resp.value))),
                                        pending.minus(msg.resp.deviceId)
                                ).invoke()
                            } else {
                                return@receive nextBehaviorCurr(
                                        replies.plus(Pair(msg.resp.deviceId, TemperatureReading.TemperatureNotAvailable)),
                                        pending.minus(msg.resp.deviceId)
                                ).invoke()
                            }
                        }
                        is QueryCommand.DeviceTerminated -> {
                            if (pending.contains(msg.deviceId)) {
                                return@receive nextBehaviorCurr(
                                        replies.plus(Pair(msg.deviceId, TemperatureReading.DeviceNotAvailable)),
                                        pending.minus(msg.deviceId)
                                ).invoke()
                            } else {
                                return@receive Behaviors.same()
                            }
                        }
                        is QueryCommand.QueryTimeout -> {
                            var res = replies
                            pending.forEach { dev -> res = res.plus(Pair(dev, TemperatureReading.DeviceTimedOut)) }
                            return@receive nextBehaviorCurr(res, emptySet()
                            ).invoke()
                        }
                        else -> Behaviors.unhandled()
                    }
                },
                { ctx, signal ->
                    when (signal) {
                        is PostStop -> {
                            ctx.log.info("DeviceGroupQuery stopped")
                            Behaviors.same()
                        }
                        else -> Behaviors.unhandled()
                    }
                }
        )
    }
}
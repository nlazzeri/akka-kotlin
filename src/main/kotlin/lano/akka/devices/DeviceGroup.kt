package lano.akka.devices

import akka.actor.TypedActor
import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.javadsl.Behaviors
import java.time.Duration

object DeviceGroup {
    interface Command
    sealed class GroupCommand : Command {
        data class DeviceTerminated(val device: ActorRef<Device.Command>, val groupId: String, val deviceId: String) : GroupCommand()
    }

    fun behavior(groupId: String, deviceIdToActor: Map<String, ActorRef<Device.Command>>): Behavior<Command> = Behaviors.setup { setupCtx ->
        setupCtx.log.info("DeviceGroup {} started", groupId)
        return@setup Behaviors.receive(
                { ctx, msg ->
                    when (msg) {
                        is SharedDeviceCommands.RequestTrackDevice -> {
                            when (msg.groupId) {
                                groupId -> {
                                    if (deviceIdToActor.containsKey(msg.deviceId)) {
                                        msg.replyTo.tell(SharedDeviceCommands.DeviceRegistered(deviceIdToActor[msg
                                                .deviceId]!!))
                                    } else {
                                        val deviceActor = ctx.spawn(Device.deviceBehavior(groupId, msg.deviceId, null), "device-$groupId-${msg.deviceId}")
                                        ctx.watchWith(deviceActor, GroupCommand.DeviceTerminated(deviceActor, groupId, msg
                                                .deviceId))
                                        msg.replyTo.tell(SharedDeviceCommands.DeviceRegistered(deviceActor))
                                        return@receive behavior(groupId, deviceIdToActor.plus(Pair(msg.deviceId, deviceActor)))
                                    }
                                }
                                else -> {
                                    ctx.log.info("I am this devicegroup: {} but not this one: {} -> im ignoring your request", groupId, msg.groupId)
                                }
                            }
                            return@receive Behaviors.same()
                        }
                        is GroupCommand.DeviceTerminated -> {
                            ctx.log.info("Device has Terminated: groupId {}, deviceid {}", groupId, msg.deviceId)
                            return@receive behavior(groupId, deviceIdToActor.minus(msg.deviceId))
                        }
                        is SharedDeviceCommands.QueryDeviceList -> {
                            if (msg.groupId.equals(groupId)) {
                                msg.replyTo.tell(SharedDeviceCommands.QueryDevicesResponse(msg.requestId, deviceIdToActor
                                        .keys))
                                return@receive Behaviors.same()
                            } else {
                                ctx.log.info("I am this devicegroup: {} but not this one: {} -> im ignoring your request with id {}",
                                        groupId, msg.groupId, msg.requestId)
                                return@receive Behaviors.unhandled()
                            }
                        }
                        is SharedDeviceCommands.QueryAllTemps -> {
                            if (msg.groupId == groupId) {
                                ctx.spawn(DeviceGroupQuery.behavior(
                                        msg.requestId,
                                        msg.replyTo,
                                        deviceIdToActor,
                                        Duration.ofSeconds(3)
                                ), "device-group-query_for_req_${msg.requestId}")
                                return@receive Behaviors.same()
                            }
                            else {
                                ctx.log.info("ignoring QueryAllTemps: wrong group id. this: $groupId, provided: ${msg.groupId}")
                                return@receive Behaviors.unhandled()
                            }
                        }
                        else -> return@receive Behaviors.unhandled()
                    }
                },
                { ctx, signal ->
                    when (signal) {
                        is TypedActor.PostStop -> {
                            ctx.log.info("stopping deviceGroup with groupId {}", groupId)
                        }
                    }
                    return@receive Behaviors.same()
                }
        )
    }
}
package lano.akka.devices

import akka.actor.testkit.typed.javadsl.ActorTestKit
import io.kotlintest.shouldBe
import io.kotlintest.specs.WordSpec
import org.junit.platform.commons.annotation.Testable

class DeviceSpec : WordSpec({
    "Device actor" should {
        "reply with empty reading if no temperature is known" {
            val tk = ActorTestKit.create()

            val probe = tk.createTestProbe(Device.ReadResponse::class.java)
            val b = tk.spawn(Device.deviceBehavior("gid", "did", null))
            b.tell(Device.Command.ReadTemp(42, probe.ref))

            val response = probe.expectMessageClass(Device.ReadResponse::class.java)
            response.requestId shouldBe 42
            response.value shouldBe null
        }

        "reply with latest temperature reading" {
            val tk = ActorTestKit.create()
            val recordProbe = tk.createTestProbe(Device.WriteAckResponse::class.java)
            val readProbe = tk.createTestProbe(Device.ReadResponse::class.java)
            val deviceActor = tk.spawn(Device.deviceBehavior("group", "device", null))

            deviceActor.tell(Device.Command.WriteTemp(1, 24.0, recordProbe.ref))
            recordProbe.expectMessage(Device.WriteAckResponse(requestId = 1))

            deviceActor.tell(Device.Command.ReadTemp(2, readProbe.ref))
            val response1 = readProbe.expectMessageClass(Device.ReadResponse::class.java)
            response1.requestId shouldBe 2
            response1.value shouldBe 24.0

            deviceActor.tell(Device.Command.WriteTemp(3, 55.0, recordProbe.ref))
            recordProbe.expectMessage(Device.WriteAckResponse(requestId = 3))

            deviceActor.tell(Device.Command.ReadTemp(4, readProbe.ref))
            val response2 = readProbe.expectMessageClass(Device.ReadResponse::class.java)
            response2.requestId shouldBe 4
            response2.value shouldBe 55.0
        }
    }
})

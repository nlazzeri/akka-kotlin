package lano.akka.devices

import akka.actor.testkit.typed.javadsl.ActorTestKit
import io.kotlintest.milliseconds
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.WordSpec
import org.junit.platform.commons.annotation.Testable

@Testable
class DeviceGroupSpec : WordSpec({
    "DeviceGroupActor shoud" should {
        val tk = ActorTestKit.create()

         "be able to register a device actor" {
            val probe = tk.createTestProbe(SharedDeviceCommands.DeviceRegistered::class.java)
            val groupActor = tk.spawn(DeviceGroup.behavior("group", emptyMap()))

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device1", probe.ref))
            val registered1 = probe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)
            val deviceActor1 = registered1.device

            // another deviceId
            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device2", probe.ref))
            val registered2 = probe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)
            val deviceActor2 = registered2.device
            deviceActor1 shouldNotBe deviceActor2

            // Check that the device actors are working
            val recordProbe = tk.createTestProbe(Device.WriteAckResponse::class.java)
            deviceActor1.tell(Device.Command.WriteTemp(0, 1.0, recordProbe.ref))
            recordProbe.expectMessage(Device.WriteAckResponse(requestId = 0))
            deviceActor2.tell(Device.Command.WriteTemp(1, 2.0, recordProbe.ref))
            recordProbe.expectMessage(Device.WriteAckResponse(requestId = 1))
            1 shouldNotBe 0
        }

        "ignore requests for wrong groupId" {
            val probe = tk.createTestProbe(SharedDeviceCommands.DeviceRegistered::class.java)
            val groupActor = tk.spawn(DeviceGroup.behavior("group", emptyMap()))

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("wrongGroup", "device1", probe.ref))
            probe.expectNoMessage(500.milliseconds)
        }

        "be able to list active devices" {
            val registeredProbe = tk.createTestProbe(SharedDeviceCommands.DeviceRegistered::class.java)
            val groupActor = tk.spawn(DeviceGroup.behavior("group", emptyMap()))

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device1", registeredProbe.ref))
            registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)
            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device2", registeredProbe.ref))
            registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)

            val deviceListProbe = tk.createTestProbe(SharedDeviceCommands.QueryDevicesResponse::class.java)
            groupActor.tell(SharedDeviceCommands.QueryDeviceList(0, "group", deviceListProbe.ref))
            deviceListProbe.expectMessage(SharedDeviceCommands.QueryDevicesResponse(0, setOf("device1", "device2")))
        }

        "be able to list active devices after one shuts down" {
            val registeredProbe = tk.createTestProbe(SharedDeviceCommands.DeviceRegistered::class.java)
            val groupActor = tk.spawn(DeviceGroup.behavior("group", emptyMap()))

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device1", registeredProbe.ref))
            val registered1 = registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)
            val toShutDown = registered1.device

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device2", registeredProbe.ref))
            registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)

            val deviceListProbe = tk.createTestProbe(SharedDeviceCommands.QueryDevicesResponse::class.java)
            groupActor.tell(SharedDeviceCommands.QueryDeviceList(0, "group", deviceListProbe.ref))
            deviceListProbe.expectMessage(SharedDeviceCommands.QueryDevicesResponse(0, setOf("device1", "device2")))

            toShutDown.tell(Device.Command.Passivate)
            registeredProbe.expectTerminated(toShutDown, registeredProbe.remainingOrDefault)

            // using awaitAssert to retry because it might take longer for the groupActor
            // to see the Terminated, that order is undefined
            registeredProbe.awaitAssert {
                groupActor.tell(SharedDeviceCommands.QueryDeviceList(1, "group", deviceListProbe.ref))
                deviceListProbe.expectMessage(SharedDeviceCommands.QueryDevicesResponse(1, setOf("device2")))
            }
        }

        "be able to collect temperatures from all active devices" {
            val registeredProbe = tk.createTestProbe(SharedDeviceCommands.DeviceRegistered::class.java)
            val groupActor = tk.spawn(DeviceGroup.behavior("group", emptyMap()))

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device1", registeredProbe.ref))
            val deviceActor1 = registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java).device

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device2", registeredProbe.ref))
            val deviceActor2 = registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java).device

            groupActor.tell(SharedDeviceCommands.RequestTrackDevice("group", "device3", registeredProbe.ref))
            registeredProbe.expectMessageClass(SharedDeviceCommands.DeviceRegistered::class.java)

            // Check that the device actors are working
            val recordProbe = tk.createTestProbe(Device.WriteAckResponse::class.java)
            deviceActor1.tell(Device.Command.WriteTemp(0, 1.0, recordProbe.ref))
            recordProbe.expectMessage(Device.WriteAckResponse(requestId = 0))
            deviceActor2.tell(Device.Command.WriteTemp(1, 2.0, recordProbe.ref))
            recordProbe.expectMessage(Device.WriteAckResponse(requestId = 1))
            // No temperature for device3

            val allTempProbe = tk.createTestProbe(SharedDeviceCommands.QueryTempResponse::class.java)
            groupActor.tell(SharedDeviceCommands.QueryAllTemps(0, "group", allTempProbe.ref))
            allTempProbe.expectMessage(
                    SharedDeviceCommands.QueryTempResponse(
                            requestId = 0,
                            temperatures = mapOf(
                                    Pair("device1", TemperatureReading.Temperature(1.0)),
                                    Pair("device2", TemperatureReading.Temperature(2.0)),
                                    Pair("device3", TemperatureReading.TemperatureNotAvailable)
                            )
                    )
            )
        }

    }
})
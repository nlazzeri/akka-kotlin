package lano.akka.devices

import akka.actor.testkit.typed.javadsl.ActorTestKit
import io.kotlintest.milliseconds
import io.kotlintest.seconds
import io.kotlintest.specs.WordSpec

class DeviceGroupQuerySpec : WordSpec({

    "DeviceGroupQuery" should {
        val tk = ActorTestKit.create()
        "return temperature value for working devices" {
            val requester = tk.createTestProbe(SharedDeviceCommands.QueryTempResponse::class.java)

            val device1 = tk.createTestProbe(Device.Command::class.java)
            val device2 = tk.createTestProbe(Device.Command::class.java)

            val deviceIdToActor = mapOf(Pair("device1", device1.ref), Pair("device2", device2.ref))

            val queryActor = tk.spawn(DeviceGroupQuery.behavior(
                    requestId = 1,
                    requester = requester.ref,
                    deviceIdToActor = deviceIdToActor,
                    timeout = 10.seconds
            ))

            device1.expectMessageClass(Device.Command.ReadTemp::class.java)
            device2.expectMessageClass(Device.Command.ReadTemp::class.java)

            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device1", 1.0)))
            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device2", 2.0)))

            requester.expectMessage(SharedDeviceCommands.QueryTempResponse(
                    requestId = 1,
                    temperatures = mapOf(
                            Pair("device1", TemperatureReading.Temperature(1.0)),
                            Pair("device2", TemperatureReading.Temperature(2.0))
                    )
            ))
        }

        "return TemperatureNotAvailable for devices with no readings" {
            val requester = tk.createTestProbe(SharedDeviceCommands.QueryTempResponse::class.java)

            val device1 = tk.createTestProbe(Device.Command::class.java)
            val device2 = tk.createTestProbe(Device.Command::class.java)

            val deviceIdToActor = mapOf(Pair("device1", device1.ref), Pair("device2", device2.ref))

            val queryActor = tk.spawn(DeviceGroupQuery.behavior(
                    requestId = 1,
                    requester = requester.ref,
                    deviceIdToActor = deviceIdToActor,
                    timeout = 10.seconds
            ))

            device1.expectMessageClass(Device.Command.ReadTemp::class.java)
            device2.expectMessageClass(Device.Command.ReadTemp::class.java)

            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device1", null)))
            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device2", 2.0)))

            requester.expectMessage(SharedDeviceCommands.QueryTempResponse(
                    requestId = 1,
                    temperatures = mapOf(
                            Pair("device1", TemperatureReading.TemperatureNotAvailable),
                            Pair("device2", TemperatureReading.Temperature(2.0))
                    )
            ))
        }

        "return DeviceNotAvailable if devices stops before answering" {
            val requester = tk.createTestProbe(SharedDeviceCommands.QueryTempResponse::class.java)

            val device1 = tk.createTestProbe(Device.Command::class.java)
            val device2 = tk.createTestProbe(Device.Command::class.java)

            val deviceIdToActor = mapOf(Pair("device1", device1.ref), Pair("device2", device2.ref))

            val queryActor = tk.spawn(DeviceGroupQuery.behavior(
                    requestId = 1,
                    requester = requester.ref,
                    deviceIdToActor = deviceIdToActor,
                    timeout = 10.seconds
            ))

            device1.expectMessageClass(Device.Command.ReadTemp::class.java)
            device2.expectMessageClass(Device.Command.ReadTemp::class.java)

            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device1", 1.0)))
            device2.stop()

            requester.expectMessage(SharedDeviceCommands.QueryTempResponse(
                    requestId = 1,
                    temperatures = mapOf(
                            Pair("device1", TemperatureReading.Temperature(1.0)),
                            Pair("device2", TemperatureReading.DeviceNotAvailable))
            )
            )
        }

        "keep reading if devices stops after already having answered " {
            val requester = tk.createTestProbe(SharedDeviceCommands.QueryTempResponse::class.java)

            val device1 = tk.createTestProbe(Device.Command::class.java)
            val device2 = tk.createTestProbe(Device.Command::class.java)

            val deviceIdToActor = mapOf(Pair("device1", device1.ref), Pair("device2", device2.ref))

            val queryActor = tk.spawn(DeviceGroupQuery.behavior(
                    requestId = 1,
                    requester = requester.ref,
                    deviceIdToActor = deviceIdToActor,
                    timeout = 10.seconds
            ))

            device1.expectMessageClass(Device.Command.ReadTemp::class.java)
            device2.expectMessageClass(Device.Command.ReadTemp::class.java)

            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device1", 1.0)))
            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device2", 2.0)))
            device2.stop()

            requester.expectMessage(SharedDeviceCommands.QueryTempResponse(
                    requestId = 1,
                    temperatures = mapOf(
                            Pair("device1", TemperatureReading.Temperature(1.0)),
                            Pair("device2", TemperatureReading.Temperature(2.0))
                    )
            ))
        }

        "stop reading on timeout and return DeviceTimedOut" {
            val requester = tk.createTestProbe(SharedDeviceCommands.QueryTempResponse::class.java)

            val device1 = tk.createTestProbe(Device.Command::class.java)
            val device2 = tk.createTestProbe(Device.Command::class.java)

            val deviceIdToActor = mapOf(Pair("device1", device1.ref), Pair("device2", device2.ref))

            val queryActor = tk.spawn(DeviceGroupQuery.behavior(
                    requestId = 1,
                    requester = requester.ref,
                    deviceIdToActor = deviceIdToActor,
                    timeout = 200.milliseconds
            ))

            device1.expectMessageClass(Device.Command.ReadTemp::class.java)
            device2.expectMessageClass(Device.Command.ReadTemp::class.java)

            queryActor.tell(DeviceGroupQuery.QueryCommand.WrappedReadResponse(Device.ReadResponse(0, "device1", 1.0)))

            requester.expectMessage(SharedDeviceCommands.QueryTempResponse(
                    requestId = 1,
                    temperatures = mapOf(
                            Pair("device1", TemperatureReading.Temperature(1.0)),
                            Pair("device2", TemperatureReading.DeviceTimedOut)
                    )
            ))
        }
    }

})